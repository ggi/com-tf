# [How to] Add a new member

Write down news content (check ospo-alliance.org/news for tone, length)

 **Publishing**

* Add markdown file file in [/content/news](https://gitlab.eclipse.org/eclipse/plato/www/-/tree/main/content/news)
* Naming pattern is `YYYYMMDD_filename.md` - where `YYYYMMDD` is publishing date
* Header format:

> title: ":wave_tone2: XYZ confirms support to the OSPO Alliance"\
> date: YYYY-MM-DDTHH:MM:SS+HH:MM\
> layout: "single"

* Summary page: Displayed: title (Bold) + first paragraph (no formatting)
* Detail page: Full text with formatting
* Date: displayed date in title header - date of the event

**Amplify**

* Post on Linkedin
  * Hashtags: #OSPOAlliance #OSPO #opensource
  * Tag related people
* Amplify on Mastodon, BlueSky, X, Forum
* Communicate news with social links to amplify (Need to confirm public ML usage as it's been scarecely used so far).
  * Private: OSPO Alliance Governance \<ossgovernance@ow2.org\>
  * Private: ospo-zone-team@framalistes.org
  * Public: ospo.zone@eclipse.org