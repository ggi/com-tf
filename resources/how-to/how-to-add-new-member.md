# [How to] Add new member sequence

**Receive statement of support** via ospo-zone-team@framalistes.org 
1. Check for non-moderated message
2. Facts and validity checking, connection with open source and OSPO focus
3. Submit to governance discussion if any doubt

**Store statement of support in NextCloud**
1. Univention Portal or https://portal.ospo-alliance.org/nextcloud
2. Folder "Signed Statement of Support" Fichiers - Nextcloud

**Publish logo**
1. Access / Login to https://gitlab.eclipse.org/eclipse/plato/www
2. Add logo file in Static / Images / partner-logos
3. Create entry for new partner in Layout / Short codes / Section-members.html 
  - Copy / Paste lines related to new partner - Change URL, Name, Logo
  - Merge Request

**Communicate on new member**
1. Recommended: subscribe to the mailing list and introduce yourself to the community.
2. Get member short description (< 500 car) and quote
3. Add related MD file in content / news
   - Prefix name is date of event (or news)
   - Picto: 👋🏼
4. Add post with (#OSPO #OSPOAlliance #OpenSource) on 
   - [ ] LinkedIn,
   - [ ] X
   - [ ] Mastodon
   - [ ] Forum
5. Welcome email on OSS Governance MLs
  - Private: OSPO Alliance Governance <ossgovernance@ow2.org>
  - Private: ospo-zone-team@framalistes.org
  - Public: ospo.zone@eclipse.org
