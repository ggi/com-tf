# Stickers

This is some guidelines to print OSPO Stickers. Recommendations are based on [Sticker Mule](), but you're free to choose any manufacturer that suits you.

## Files
File to use are located under the `logos/OSPO_Alliance_Logo_RGB` subdirectory

- [SVG Format SVG](https://gitlab.ow2.org/ggi/com-tf/-/blob/main/resources/logos/source/OSPO_Alliance_Logo_wide.svg)
- [Transparent PNG Format](https://gitlab.ow2.org/ggi/com-tf/-/blob/main/resources/logos/OSPO_Alliance_Logo_RGB/OSPO_Alliance_Logo_wide_512x241.png)

## Characteristics

- Size: 81 mm × 25 mm
- Shape: Rounded corners
- Ask for white space around the logo to be trimmed to 2 mm

## Illustration

![example of a sticker](OSPO_Alliance_sticker_example.jpg)