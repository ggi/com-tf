Hi, I am Sebastien Lejeune, Open Source Advocate at Thales working at the Group Technical Directorate. 

## Context 

Thales is a multinational group with french roots that designs, develops and manufactures electronical systems as well as devices and equipment for the aerospace, defence, transportation and security sectors. 

Open-source context: 
* Open Source Software & Hardware are strategic pillars for Thales, we want to foster collaborations between the actors of these communities through our GitHub organization: https://github.com/ThalesGroup
* We did set an OSPO up back in 2020 and we have put in place a unified process & tooling to help collaborators to contribute to existing open source project and to share Thales assets in open source
* Open-source is sponsored by the top management and clearly defined in the group strategy

## My pain points

I needed:
* to make people aware about this global initiative across the group
* to convince middle management to encourage their team to contribute
* to scale up, having more contributors inside Thales but also more external ones

## How I did it

I joined the OSPO Alliance in 2022 and became an active committer after few months.

I instanciated the GGI board into our internal Gitlab at Thales and it helped me to have an overview about all the activities I had already achieved but also a lot of relevant resources to legitimate Thales Open Source strategy and to share them with different involved people.

## Achievements since 2020
* We set up an OSPO
* We set up tooling & processes to ensure that all collaborators can easily publish assets in open-source and also can contribute to any existing open-source project.
* We defined a clear documentation shared with everyone across Thales 
* We expanded our open source foundations memberships: Eclipse, Linux Foundation, CNCF, OpenHardware Group
* We are active and involved in several open-source working groups: Systematic Open Source Hub, Eclipse  & CNCF projects, Good Governance Initiative