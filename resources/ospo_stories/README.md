# OSPO Stories

This is an on-going, collaborative work, please contribute by cloning and editing.

OSPO Stories include feedback from GGI implementations as well as broader feedback about OSPOs, not particularly linked to the GGI.

## First Draft (Hervé Pacault, 2021-06-30)

* **There is, in my organization, a team of software developers.  Most likely, even though I may not be fully aware of it, they are already using open source languages and libraries. However I am sure that the code remains within the company.**
  Encouraging developers to contribute back to the open source communities would increase their motivation and skills.  Legal aspects are also to be considered in case of contribution.

* **I feel frustrated with some applications that are key for my business and that are sourced from software companies : they are over complex, I do not have much control over the functional roadmap, it takes too much time to fix bugs, despite the high cost of the license and support.**
  Switching from a traditional sourcing approach to an open source approach is a change in culture

* **I developed applications embedding open source components, that I deployed in my affiliates/subsidiaries and on my customer premises (mobile apps).**
  This is a case of Distribution.  Legal aspects are to be considered.

* **I believe that the market will thrive if all players on this market, be they suppliers, customers or competitors, adopt the same interfaces for their IT (case of 5G, Internet of Things…) and have their products and services easily interoperate.**
  This is leveraging open source to structure an ecosystem (standardization).  This is open innovation.  Culture and Legal aspects are to be considered.

* **I am a software vendor or integrator.  I believe that open source is a key market trend and I want to leverage it to develop sales and market share.**
  Identify the various business models to create value through open source

* **My organization (Defense industry, government…) processes sensitive data, and I want to be sure of the confidentiality and security of the data.**
  Open  source could be part of the solution.  But one should keep in mind that Open source code is not necessarily malware-free.  Security checks remain a necessity.  Legal aspects as well.
