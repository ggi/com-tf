
**OSPO Stories** aim to **honor individuals** who have **actively engaged, learned**, and **shared their insights** gained from their experiences with **OSPO** and the **Good Governance Initiative**. These individuals have **contributed to shaping the trajectory** of their organisations towards success through **open source**.”


You are interested in Open Source and believe a structured and good governance is key to foster adoption success and establish an OSPO, come and engage with us: 

**Join the community** [forum.ospo-alliance.org](forum.ospo-alliance.org) a safe and friendly space to discuss everything related to OSPOs. Learn from other experiences thru our [OnRamp sessions](OSPO-alliance.org/onramp).

**Support us** https://ospo-alliance.org/membership/ to express your support for the initiative. 

**Engage** https://ospo-alliance.org/contribute/ get involved in the project.