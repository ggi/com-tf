# Boilerplates for the OSPO Alliance

## Short version (~200 char)
The OSPO Alliance is an open community that brings and shares guidance to organisations looking to manage the usage, contribution to, and publication of, open source software.

## Long version (~650 char)
The **OSPO Alliance** is an open community that brings and shares guidance to organisations willing to professionally manage the usage, contribution to and publication of open source software.
The **Good Governance Initiative** (GGI) proposes a methodological framework to assess and improve open-source trust, awareness and strategy within organisations.
The **OSPO OnRamp** meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source.
GGI and the OnRamp series are key initiatives from the OSPO Alliance.
