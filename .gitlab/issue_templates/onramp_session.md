# On Ramp session preparation
<!-- Use this to track the preparation of an upcoming OnRamp session -->

- Title: **Insert title of the presentation here**
- Speaker Name: **Insert speaker name here**
- Speaker Company: **Insert speaker name here**
- Date: **Insert session date here**

## Required information

Contact the speaker and ask for

- [ ] Fullname
- [ ] Position 
- [ ] Company exact name
- [ ] A small bio to present the speaker during the OnRamp session
- [ ] A photo for the speaker card to be used on social networks
- [ ] Title and abstract of the talk
- [ ] Twitter handle of the speaker and the company
- [ ] Mastodon handle of the speaker and the company
- [ ] LinkedIn profile of the speaker and the company
- [ ] website of the speaker and the company

## Communication

Advertise on the following networks / website

- [ ] Twitter
- [ ] LinkedIn (create an **Event**, not a post)
- [ ] Mastodon
- [ ] Website news section (aka Eclipsed Newsroom)
- [ ] Ospo.Zone ML
- [ ] Next OnRamp session slides

## Slides

- [ ] Prepare intro and outro slides, including
  - [ ] Latest news from the Alliance (translations, TF, new members, etc.)
  - [ ] Introduction slide
  - [ ] Next sessions
- [ ] Ask speaker for his/her slides at least 2 days in advance

## Post meeting

- [ ] Check video replay on BBB
- [ ] Update website: move abstract to previous meetings and resources pages by adding link to slides and replay
- [ ] Communicate about the replay and thank the speaker on social networks
  - [ ] Twitter
  - [ ] LinkedIn
  - [ ] Mastodon
  - [ ] Website news section (aka Eclipsed Newsroom)
  - [ ] Ospo.Zone ML

/assign @flzara
/label ~"OnRamp::Backlog"
