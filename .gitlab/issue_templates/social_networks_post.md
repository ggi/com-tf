# Social Network Post for OSPO Alliance
<!-- Use this to request a post on social networks -->

## Where
<!-- Put an X where you where do you want to post it -->
- [ ] Twitter 
- [ ] Mastodon
- [ ] LinkedIn

## Content
<!--
   Please note that these three social networks
   - have different size limits 
   - do not handle @usernames the same way
-->
### Twitter content
<!-- 
  - Size: 280 chars max
  - Pix: 4 max
  - Usernames format: @username
-->


### Mastodon content
<!-- 
  - Size: 500 chars max
  - Pix: 4 max
  - Usernames format: @username@server.example
-->


### LinkedIn
<!-- 
  - Size: 3,000 chars max
  - Pix: 1 max
  - Usernames format: @Firstname Surname
-->


### Images
<!-- Any images to add to the post. 4 max -->

## Date and time
<!--
  When to post it ideally. Please note that all Social Networks 
  do not offer the possibility to schedule a post. Il will be done 
  on a best effort basis. Please use CET times, 24h format.
-->

- YYYY-MM-DD
- HH:MM


---- 
/ping @flzara @bbaldassari @cnuel
