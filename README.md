# OSPO Alliance Communication Task Force

This is a GitLab project to coordinate the work done by the Communication Task Force of the GGI. It is mainly used to 

- share and store files between its members
- work collaboratively on deliverables (taht would be then proposed to the GGI community)
- keep track of tasks and their status using issues

## Meetings

Meetings take place every other Thursday from 14:00 to 15:00 CEST on 🎥 https://bbb.opencloud.lu/rooms/flo-jqs-nir-elb/join

Minutes are located on this pad: https://pad.castalia.camp/p/communication-task-force-veg0arf

## Useful documents

**Event Plan**: GGi Event Plan is visible and can be edited at this link: https://nextcloud.ow2.org/index.php/s/tnQqpADp4Emo8PC

**Press & Media Plan**: GGI Media Plan is visible and can be edited at this link: https://nextcloud.ow2.org/index.php/s/SHF2nd7GzyfAxiS

**Guidelines to print a paperback copy of the handbook**: https://nextcloud.ow2.org/index.php/s/6Kg5DrSRnmATRCQ

**Guidelines to print OSPO Alliance stickers**: see [stickers.md](resources/stickers.md)

## How-to

- [Add a new member](/resources/how-to/how-to-add-new-member.md)
- [Add and amplify a news](/resources/how-to/how-to-add-amplify-news.md)

## Members

- [Catherine Nuel](https://gitlab.ow2.org/cnuel)
- [Fred Aatz](https://gitlab.ow2.org/fredaatz)
- [Boris Baldassari](https://gitlab.ow2.org/bbaldassari)
- [Florent Zara](https://gitlab.ow2.org/flzara)
- [Antonio Conti](https://gitlab.ow2.org/antonio)
- [Valentina Del Prete]
- [Silona Bonewald](https://gitlab.ow2.org/silona) 
