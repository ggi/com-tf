# Version ENG
Invitation: OSPO Alliance Breakout Session

Dear Open Source Enthusiast,

We are thrilled to announce the upcoming Breakout Session titled “OSPO, You Can Be Heroes!” as part of the OW2con’2024 event (https://www.ow2con.org/view/2024/Breakout_Sessions/Open_Source_Governance). 

This session's intent is to engage discussion with the audience fostered by few OSPO heroes and a moderator. 
Inside-out discussion about why we don't have OSPO's developing everywhere, public, private, academia, small or large organization.

We are looking for few guest speakers  to kick off the session by sharing their experience (good, bad, ugly - in 3 minutes, no slides), you/we are heroes .. But why are we so few ?

Date & Time: June 11, 16:40–17:40 PM Location: Orange Labs
Session: Breakout Session: OSPO, you can be heroes! - OW2con
How: No prep (just be ready to be short, impactful and even provocative), come with your energy and beliefs!!

# Version FR *
Invitation : Workshop de l'OSPO Alliance
 
Hello,
 
Nous sommes ravis d'annoncer le workshop intitulé « OSPO, You Can Be Heroes ! » dans le cadre de l'événement OW2con'2024. https://www.ow2con.org/view/2024/Breakout_Sessions/Open_Source_Governance 
 
L'objectif de cette session est d'engager la discussion avec l'audience, encouragée par quelques héros du monde OSPO et un modérateur. 
Nous voulons partager, creuser les raisons pour lesquelles nous n'avons pas d'avantage d'OSPO qui se développent partout, dans le public, le privé, le milieu universitaire, les petites ou les grandes organisations.
 
Nous sommes à la recherche de quelques conférenciers invités pour lancer la session en partageant leur expérience (bonne ou mauvaise - en 3 minutes, pas de slides), vous/nous sommes des héros.. Mais pourquoi sommes-nous si peu nombreux ?
 
Date et heure : 11 juin, de 16 h 40 à 17 h 40 Lieu : Orange Labs
Session : Workshop : OSPO, vous pouvez être des héros ! - OW2con
Comment : Pas de préparation (soyez juste prêt à être bref, percutant et même provocateur), venez avec votre énergie et vos convictions !!