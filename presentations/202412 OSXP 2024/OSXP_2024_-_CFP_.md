# OSXP 2024 - CFP Proposals

## Topic 1 (Tech) Breaking Language Barriers: Open Source Good Governance Initiative Goes Global (aka Weblate)

### Abstract
In an interconnected world, open source projects thrive on collaboration and inclusivity. This talk proposal explores how Technical Translations and Weblate play a pivotal role in breaking down language barriers, making open source content accessible to diverse audiences worldwide.
### Key Points
	1. Guide to Good Practice in Open Source Governance
		○ Empowering contributors: How handbooks benefit from global participation.
		○ Accessibility as a priority: Ensuring content reaches as many people as possible.
		○ Collaboration across borders: The power of shared knowledge.
	2. Why Language Matters: Accessibility and Compliance
		○ Mother tongue readership: A broad issue that transcends borders.
		○ Aligning with public services: Providing information in local languages.
		○ Compliance with international standards.
	3. Weblate: The Open Source Translation Tool
		○ Translation made easy: Weblate’s role in the open process.
		○ Repeatable workflows: Ensuring consistent translations.
		○ Tooling for success: Leveraging Weblate’s features.
	4. Collaborative Translation Process
		○ Engaging contributors: Collaborative translation efforts.
		○ Proofreading and quality control.
		○ Leveraging translation engines and individual suggestions.
		○ Automated content generation in multiple languages.
	5. Kickstarting New Languages
		○ Quick setup: How-to guide for launching translations.
		○ Minimizing barriers: Making it accessible for all.
	6. Current Progress
		○ Available languages: FR (French), ENG (English), DE (German), PO (Polish), ESP (Spanish), IT (Italian), NED (Dutch), CHI (Chinese).
### Conclusion
Join us in the journey to bridge linguistic gaps, foster collaboration, and create a truly global open source community. Let’s break down language barriers together!


## Topic 2 (Tech) Leveraging Open Source Software for Enhanced Governance and Innovation (aka Gitlab & GitHub)

### Abstract:
In an era where software is ubiquitous across all organizational functions, open source solutions have emerged as a dominant force reshaping the landscape of software development and deployment. This talk will delve into the transformative role of Technical Open Source Program Offices (OSPO) as heroes championing good governance through their organisations based on tools such as My GGI Board now accessible via Gitlab/Github platforms.
### Key Points:
	1. Software Everywhere:
		○ The pervasive nature of software in organizations has led to open source methodologies consuming traditional software paradigms.
		○ Elevating open source strategies is crucial for recognizing them as essential components of supply chains and as catalysts for business innovation.
	2. Good Governance Handbook:
		○ A comprehensive Good Governance Handbook provides pivotal insights for establishing successful OSPOs that can navigate the complexities of open source ecosystems.
		○ It aims to guide OSPOs toward effective practices and sustainable growth.
	3. Dashboard as Governance Radar:
		○ Implementing dashboards serves as a governance radar, aligning business objectives with value-driven metrics.
		○ These dashboards act as a NorthStar, guiding organizations toward measurable success.
Relevance: This presentation will highlight how integrating open source strategy within an organization’s, guided by My GGI Board, not only fuels technological advancement but also serves as a key differentiator in today’s competitive market. By showcasing practical integration with Gitlab and Github, we demonstrate that effective governance is no longer confined to corporate settings but extends into developers’ homes, fostering community-driven progress.
### Real world examples: THALES, SOCGEN, …
### Conclusion: 
Attendees will leave equipped with actionable strategies to up-level their organization’s approach to open source software, harnessing its potential not just as a tool but as an indispensable ally in steering business growth through principled governance practices.


## Topic 3 (Bus) Unlocking the Strategic Value of Open Source in Business

### Abstract: 

Open source software is a cornerstone of modern technology, driving innovation and efficiency. Despite its widespread adoption, challenges persist for open source advocates. This talk explores the strategic value of open source and addresses key hurdles faced by OSPO champions, including real world examples from companies and administrations.

1. Formalizing OSPO: Many organizations lack a structured approach to managing open source
2. Educating Stakeholders: Champions must educate various stakeholders, including executives and business units, requiring a comprehensive pedagogical approach.
3. Executive Alignment: Executives must recognize open source solutions as potential enterprise-grade software and open source principles as innovation and talent acquisition drivers.
4. Beyond Cost: The broader value of open source, is often overlooked.
5. Strategic Role: Businesses must recognize the strategic role of open source in influencing business models and market positioning.

### Full Description:
#### Key Topics
1. Formalizing OSPO (Open Source Program Office):
   - While some organizations have established formal OSPOs, many lack a structured approach to managing open source initiatives.
   - Champions play a crucial role but often face time constraints.
2. Educating Stakeholders:
   - Open source champions must educate various stakeholders, including executives, marketing teams, business units, sourcing and HR.
   - A comprehensive pedagogical approach is needed to broaden understanding beyond technical teams.
3. Executive Alignment and Support:
   - Lack of alignment from top executives hinders strategic integration.
   - CTOs should view open source solutions as enterprise-grade software with the potential for industry-level support.
   - Executives must recognize open source as a driver of innovation, collaboration, and talent acquisition.
4. Beyond Cost Considerations:
   - While cost savings are understood, the broader value of open source is often overlooked.
   - Open source fosters collaboration, responsibility, and shared innovation beyond technology stacks.
5. Strategic Role of Open Source:
   - In the era of software disruption (cloud, AI), businesses must recognize the strategic role of open source.
   - It influences business models, market positioning, and talent attraction.

Conclusion:  
Bridging the gap between technology-centric views and strategic business perspectives is essential. By investing in education, executive alignment, and a holistic understanding of open source, organizations can fully leverage its potential for growth and innovation.

#### Stories:
SNCF, THALES, VDP, ORANGE ..
